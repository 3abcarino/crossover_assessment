﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrossoverAssessment.Core.Helpers;
using CrossoverAssessment.Data;
using CrossoverAssessment.Data.DbModels;

namespace CrossoverAssessment.Services.Services
{
    public class ActivityServices : IActivityServices
    {
        /// <summary>
        /// Add activity
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="value"></param>
        /// <param name="PostTimeForTesting"></param>
        /// <returns></returns>
        public bool AddActivity(string Name, decimal value, DateTime? PostTimeForTesting = null)
        {
            try
            {
                if (Name.IsNullOrEmptyWithTrim())
                    return false;

                ActivityModel model = new ActivityModel
                {
                    ActivityName = Name.ToLower(),
                    Value = (int)Math.Round(value),
                    PostTime = PostTimeForTesting ?? DateTime.Now
                };
                return StorePoint.AddActivity(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get total of activity
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="ThresholdHours"></param>
        /// <returns></returns>
        public int GetTotal(string Name, int ThresholdHours = 12)
        {
            try
            {
                if (Name.IsNullOrEmptyWithTrim())
                    return 0;

                var total = StorePoint.GetAll(Name.ToLower(), ThresholdHours).Sum(x => x.Value);
                return total;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
