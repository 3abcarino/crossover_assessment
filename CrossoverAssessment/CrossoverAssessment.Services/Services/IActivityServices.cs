﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrossoverAssessment.Services.Services
{
    public interface IActivityServices
    {
        bool AddActivity(string Name, decimal value, DateTime? PostTimeForTesting = null);
        int GetTotal(string Name, int ThresholdHours = 12);

    }
}
