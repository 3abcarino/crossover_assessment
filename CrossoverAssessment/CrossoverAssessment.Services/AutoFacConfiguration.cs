﻿using Autofac;
using CrossoverAssessment.Services.Services;
using System;

namespace CrossoverAssessment.Services
{
    public class AutoFacConfiguration : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            #region ActivityServices
            builder.RegisterType<ActivityServices>().As<IActivityServices>();
            #endregion
        }
    }
}
