﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace CrossoverAssessment.Core.Helpers
{
    public static class ExtensionMethods
    {
        public static bool IsNullOrEmptyWithTrim(this string str)
        {
            if (str == null || str.Trim() == "")
                return true;
            return false;

        }

        public static List<string> GetMessegesErrors(this ModelStateDictionary model)
        {
            var errors = new List<string>();
            foreach (var modelState in model.Values)
            {
                foreach (var error in modelState.Errors)
                {
#if DEBUG
                    errors.Add(!(string.IsNullOrWhiteSpace(error.ErrorMessage)) ? error.ErrorMessage : error.Exception.Message);
#else
                errors.Add(!(string.IsNullOrWhiteSpace(error.ErrorMessage)) ? error.ErrorMessage : "Server error - Please contact us.");
#endif
                }
            }
            return errors;
        }

        public static string GetMessegesErrorsSummary(this ModelStateDictionary model)
        {
            string validationErrors = "";
            var errors = model.GetMessegesErrors();
            validationErrors = String.Join("<br/>", errors);
            return validationErrors;
        }
    }

    
}
