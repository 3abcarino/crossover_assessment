﻿using CrossoverAssessment.Data.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CrossoverAssessment.Data
{

    /// <summary>
    /// Use this class as store point for data
    /// </summary>
    public static class StorePoint
    {
        //Save activites in static list
        private static List<ActivityModel> Activities = new List<ActivityModel>();


        /// <summary>
        /// Add new activity
        /// </summary>
        /// <param name="Activity">Activity model</param>
        /// <returns>true = success false = fail</returns>
        public static bool AddActivity(ActivityModel Activity)
        {
            try
            {
                if (Activity == null)
                    return false;

                Activities.Add(Activity);
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        

        /// <summary>
        /// Get all activities by name 
        /// </summary>
        /// <param name="ActivityName">activity name</param>
        /// <param name="ThresholdHours">threshild hours defualt 12</param>
        /// <returns>List of activity model</returns>
        public static IEnumerable<ActivityModel> GetAll(string ActivityName, int ThresholdHours = 12)
        {
            try
            {
                IEnumerable<ActivityModel> result = new List<ActivityModel>();
                if (ActivityName == null)
                    return result;

                ActivityName = ActivityName.ToLower();
                DateTime now = DateTime.Now;
                result = Activities.Where(x => x.ActivityName == ActivityName && (now - x.PostTime).TotalHours < ThresholdHours);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
