﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CrossoverAssessment.Data.DbModels
{
    public class ActivityModel
    {
        public string ActivityName { get; set; }
        public int Value { get; set; }
        public DateTime PostTime { get; set; }
    }
}
