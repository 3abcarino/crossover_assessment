﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CrossoverAssessment.Data.DTO
{
    public class PostActivityDTO
    {
        [Required]
        public decimal Value { get; set; }
    }
}
