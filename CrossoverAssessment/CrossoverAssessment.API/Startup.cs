﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using CrossoverAssessment.Core.Helpers;
using CrossoverAssessment.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using IContainer = Autofac.IContainer;

namespace CrossoverAssessment.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .ConfigureApiBehaviorOptions(options =>
                {
                    //Custom bad request handler
                    options.InvalidModelStateResponseFactory = actionContext =>
                    {
                        var errorsAjaxResult = actionContext.ModelState.GetMessegesErrorsSummary();
                        var result = new JsonResult(errorsAjaxResult)
                        {
                            StatusCode = (int)HttpStatusCode.OK
                        };
                        return result;
                    };
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);


            // swagger configuration
            services.AddSwaggerGen(c =>
            {
                c.ResolveConflictingActions(x => x.First());
                c.SwaggerDoc("v1", new Info { Title = "Crossover API", Version = "v1" });
            });


            // Registe our services with Autofac container
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule(new AutoFacConfiguration());
            builder.Populate(services);
            IContainer container = builder.Build();

            //Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(builder =>
              builder.AllowAnyOrigin()
                     .AllowAnyHeader()
                     .AllowAnyMethod());
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("../swagger/v1/swagger.json", "Crossover V1");
            });


            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
