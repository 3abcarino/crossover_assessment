﻿using CrossoverAssessment.Data.DTO;
using CrossoverAssessment.Services.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrossoverAssessment.API.Controllers
{
    [Route("api/activity")]
    [ApiController]
    public class ActivityController : ControllerBase
    {
        private readonly IActivityServices _activityServices;

        public ActivityController(IActivityServices activityServices)
        {
            _activityServices = activityServices;
        }

        // POST api/activity/{key}
        [HttpPost("{key}")]
        public JsonResult Post([FromBody] PostActivityDTO model)
        {
            try
            {
                var key = HttpContext.Request.Path.Value.ToString().Replace("/api/activity/", "");
                if (_activityServices.AddActivity(key, model.Value))
                    return new JsonResult("");
                else
                    return new JsonResult($"Failed to post value for activity {key}");
            }
            catch
            {
                return new JsonResult("Failed to post value");
            }
           
        }

        // GET api/activity/{key}/total
        [HttpGet("{key}/total")]
        public JsonResult Get(string key)
        {
            try
            {
                var result = new { Value = _activityServices.GetTotal(key) };
                return new JsonResult(result);
            }
            catch
            {
                return new JsonResult($"Failed to get total for {key}");
            }
        }
    }
}
