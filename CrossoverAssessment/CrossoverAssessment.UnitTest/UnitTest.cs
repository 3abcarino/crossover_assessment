using CrossoverAssessment.Data.DTO;
using CrossoverAssessment.Services.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CrossoverAssessment.UnitTest
{
    [TestClass]
    public class UnitTest
    {

        IActivityServices _activityServices;

        [TestInitialize]
        public void Setup()
        {
            _activityServices = new ActivityServices();
        }

        /// <summary>
        /// Add activity successfully
        /// </summary>
        [TestMethod]
        public void AddActivityTest()
        {
            var result = _activityServices.AddActivity("activity_1", 10);
            Assert.IsTrue(result == true);
        }

        /// <summary>
        /// Test add null activity -> not added 
        /// </summary>
        [TestMethod]
        public void AddNullActivityTest()
        {
            var result = _activityServices.AddActivity(null, 10);
            Assert.IsTrue(result == false);
        }

        /// <summary>
        /// Test add empty activity -> not added
        /// </summary>
        [TestMethod]
        public void AddEmptyActivityTest()
        {
            var result = _activityServices.AddActivity("", 10);
            Assert.IsTrue(result == false);
        }

        /// <summary>
        /// Test add 3 values for same activity and return total
        /// </summary>
        [TestMethod]
        public void AddThreeNewActivitiesAndGetTotalForAll()
        {
            _activityServices.AddActivity("activity_2", 10);
            _activityServices.AddActivity("activity_2", 20);
            _activityServices.AddActivity("activity_2", 30);

            var total = _activityServices.GetTotal("activity_2");
            Assert.IsTrue(total == 60);
        }

        /// <summary>
        /// Test Prune 12 hr ago data
        /// </summary>
        [TestMethod]
        public void AddPrune12hrData()
        {
            _activityServices.AddActivity("learn_more_page", 16, DateTime.Now.AddHours(-12));
            _activityServices.AddActivity("learn_more_page", 5);
            _activityServices.AddActivity("learn_more_page", 32);
            _activityServices.AddActivity("learn_more_page", 4);

            var total = _activityServices.GetTotal("learn_more_page");
            Assert.IsTrue(total == 41);
        }


        /// <summary>
        /// I used this method to measure time response for post endpoint for 10 calls
        /// and calulate mean time and standard deviation 
        /// </summary>
        [TestMethod]
        public void ComplxityTest()
        {
            List<double> timespanList = new List<double>();
            for (int i = 0; i < 10; i++)
            {
                var startTime = DateTime.Now;
                var client = new RestClient("https://localhost:44308/api/");
                var request = new RestRequest("activity/activity_4")
                    .AddJsonBody(new PostActivityDTO
                    {
                        Value = 10
                    });
                var response = client.Post<string>(request);
                var endTime = DateTime.Now;
                var ts = endTime - startTime;
                timespanList.Add(ts.TotalMilliseconds);
            }
            var mean = timespanList.Average();
            var sumOfSquares = timespanList.Select(x => (x - mean) * (x - mean)).Sum();
            var sdev = Math.Sqrt(sumOfSquares / timespanList.Count());
            Assert.IsTrue(true);
        }
    }
}
